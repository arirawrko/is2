from django.contrib import admin
from django.contrib.admin import register

from proyecto.models import Proyecto


@register(Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)
    list_display = ('nombre', 'duracion', 'user')
    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['nombre', 'duracion', 'user']
        })
    ]
    suit_form_tabs = (
        ('general', 'General')
    )
