from django.contrib.auth.models import User
from django.db import models


class Proyecto(models.Model):
    """Esto es un comentario para pydoc"""
    nombre = models.CharField(null=False, blank=False, max_length=100)
    duracion = models.PositiveIntegerField(null=False, blank=False, default=1)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

